//lesson proper 

/*
 -javascript is a scripting programming language that enables you to make interactive web pages

*/


/* Comments

There are two ways to create comments:
 - // or ctrl + / - creates a single comment
 - /* or ctrl + shift + / - creates a multiline comment
 comments adds notes and markers for our code

 */

 console.log('hello everyone');

 //console.log() will allow us to show/display data in our console

 console.log(1+1);

 console.log('Ashley');
 console.log('pizza');

 /*Syntax and statements
 statements are instructions/expressions we add to our program which will then be communicated to our computers

 we ususally end our statements in a semicolon ; and thsi applies not only in JS but also in other programming languages.However in JS, it is not required


 syntax in programming , is a set of rules that describes how statements or instructions are properly made or constructed


 */

 console.log('i end my statement without semi colon')

/*Variables

in html, elements are containers for text and other elements, however in js we have variables as the containers of data

this then allow us to save data within our script/program

to create variable , we use let keyword 
an assignment operator(+) to store data into the variable

syntax:
let variableName = "data (value)"
*/

let name = "Thonie";

console.log(name);

let num = 5;
let num1 = 10;

console.log(num);
console.log(num1);

console.log(num + num1);
console.log("thonie's angels");


//display the data of miltiple variables in a single console.log
console.log(name, num, num1)

//you cannot use/display the value of a variable that is not yet created or declared
//console.log(num2)

/*
tHERE ARE  two steps when creating a variable:

1. Declaration
   - it is when you create the variable using the let keyword
   eg. let num = 5;
2.Initialisation
   - is when we assign the initial data/value using an assignment operator after the variable has been declared

   eg. let num;


*/

/* myVariable ;
console.log(myVariable);*/

/*
not defined vs undefined
   

   not defined is an error, it means you were using a variable that has not been declared.

   undefined means that the variable declared but there was no initial value

   can we create/declare variables without assigning an initial value?
   yes , however the value of the variable will be undefined
*/

let myVariable2;
console.log(myVariable2);
// you can assign new value to a variable after it has been declared using the assignment operator (-)

myVariable2 = "New Value";
console.log(myVariable2);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

//you can update the value of a variable that has been declared
bestFinalFantasy = "fIANAL fANTASY VII";
console.log(bestFinalFantasy);

bestStarWars = "Star Wars 8";
//this will work but is not a good practice to create or declare and initialise a variable without the let keyword.I t will automatically create a variable
console.log(bestStarWars);

/*
variable vs constant
variable are value that can change over time in a program
*/

let myVariable3;
console.log(myVariable3);
myVariable3 = "My new Variable";
console.log(myVariable3);

//cONSTANT IS A CONTAINER FOR DATA  like a variable, however constant value do not and shd not be changed.Therefore you cannot create a constat without initialisation or initial value

const myConstant = "Constatnt Value";

/*myConstant = "Updated Value";
console.log(myConstant);*/

/*Take away:
  - we can change the value of a variable
  - we cannnot change/update the value of a constant
*/

const p1 = 3.1416;
const boilingPointCelcius = 100;

/**/

let role = "Manager";

role = "Director";
name2 = 'Juan Dela Cruz';
const tin = '12444-1230';

console.log(role, name2, tin);

/*
conventions in creating variables /constants

variables and constants are named in small caps because there are other js keywords that start in capital letters

we do not name variables with spaces.FOr variable names with multiple words we can use the following"
   - camelCase
      - camelCase is a covention of writing variables by adding the first word in small caps and the following words starting with capital letter but still no use of spaces
        Eg .  lastName , boilingPoint

        vaiable names define the values they contain
        Name your variables appropriately for the value they contain
*/

let user = "0965766";

let person = "Pikachu";

/* Data typres

In most programming languages we have to declare the type of data before we are able to declare the type of data before we are able to create a variable and store data in it

however in javascript does not require this.
To create data with particular types , we use literals to create them.
To create strings we use string literals like '' or ""
objects are created with object literals = {}
arrays are created with array literals = []

*/

console.log('thonie');

// let address = province + "," + country;
// console.log(address);

let state = "Carlifornia";
let country1 = "U. S. A.";
let country3 = "Philipines"
let state1 = "Florida";
let province1 = "Rizal";
let city = "Tokyo";
let city1 = "Copenhagen";
let country2 = "Japan";

let countryCity1 = province1 + ", " + country3 ;

console.log(countryCity1);

let countryCity2 = state + ", " + country1 ;

console.log(countryCity2);

let countryCity3 = city + ", " + country2 ;

console.log(countryCity3);

/*
Numbers/Integers*/
// Integers are number data which is used for mathematical operations.

let numString1 = "5";
let numString2 = "6";
let number1 = 10;
let number2 = 6;

console.log(numString1 + numString2);
//"56 both are numeric strings and properly added"

console.log(number1 + number2);
//16 both operands are number types and properly added

let number3 = 5.5;
let number4 = .5;

console.log(number1 + number4); //10.5
console.log(number3 + number4);

//when the addition operator is used on numbers it results to the proper mathematical operation and we get the sum, however when + is used on strings, it will concatenate the string instead.

//Note : When a number/integer is added to a string , it results to concatenation

console.log(numString1 + number1);
console.log(number2 + numString2);


/*Boolean (true or false)*/
// is usually used for logical operations or for if else condition
//When creating a variable which will contain boolean, the variable is usually a yes or no question

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Stephen Curry an MVP " + isMVP);
console.log("Is he the current admin? " + isAdmin);

/*Arrays
 Arrays are special kind of data type to store multiple values.

 Arrays can actually store data with different type, however , that is not a good practice, because arrays are ususally used to store multiple values of the SAME data type

 Values in an array are separated by commas

 Arrays are created using an array literals or square brackets[]

*/

let array1 = ["Goku", "Gohan", "Vegeta", "Trunks", "Boly"];
let array2 = ["One Punch Man", "Saltana", true, 5000];

console.log(array1);
console.log(array2);

// in future sessionwe will learn about methods which will allow us to manipulate arrays, having an array with multiple data type may obstruct to the proper use of these methods

/*Objects

    objects are another special kind of data used to mimic the real world items/objects

    Used to create a complex data to contain pieces of information that relate to each other

    Each field in an object is called property.Properties are separated by a comma

    Each property is pair of key:value
    let hero ={
        heroName: "One Punch Man",
        realName: "Saitana",
        powerLevel: 5000,
        isActive: true
        }
*/

let hero ={
        heroName: "One Punch Man",
        realName: "Saitana",
        powerLevel: 5000,
        isActive: true
        }

  console.log(hero);

  /*
  Mini Activity

  Create a variable with a group of data
   The group of data should
  */


  let bandMembers = ["Harry Styles", "Liam Payne", "Louis", "Zayn Malik"];

  let zuittDev = {
  	firstName: "Ashley",
  	lastName: "Hwacha",
  	isDeveloper: true,
  	age:27
  };

  console.log(bandMembers, zuittDev);